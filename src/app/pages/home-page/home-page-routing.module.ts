import { HomePageComponent } from './home-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Homepage2Component } from './homepage2/homepage2.component';
import { NavGuard } from '../../auth/nav.guard';
import { Homepage3Component } from './homepage3/homepage3.component';

const routes: Routes = [
    {
        path: '',
        // data: { shouldReuse: false, key: 'home' },
        component: HomePageComponent,
        // canActivate: [NavGuard],
        // canActivateChild: [NavGuard],
        children: [
            {
                path: 'page-one',
                loadChildren: './pages/page-one/page-one.module#PageOneModule',
            },
            {
                path: 'home2',
                component: Homepage2Component,
                canActivate: [NavGuard],
            },
            {
                path: 'home3',
                component: Homepage3Component,
                canActivate: [NavGuard],
            },
            {
                path: '',
                redirectTo: 'home2',
                pathMatch: 'full',
            },
            {
                path: '**',
                redirectTo: 'home3',
                pathMatch: 'full',
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule {}
