import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { Homepage2Component } from './homepage2/homepage2.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CustomModule } from '../custom.module';
import { Homepage3Component } from './homepage3/homepage3.component';

@NgModule({
    declarations: [HomePageComponent, Homepage2Component, Homepage3Component],
    imports: [
        CommonModule,
        HomePageRoutingModule,
        AngularFontAwesomeModule,
        CustomModule,
    ],
})
export class HomePageModule {}
