import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { JsPdfService } from "../../../core/services/print/jspdf.service";

@Component({
  selector: "app-homepage3",
  templateUrl: "./homepage3.component.html",
  styleUrls: ["./homepage3.component.scss"]
})
export class Homepage3Component implements OnInit {
  constructor(private router: Router, private jsPdfPrint: JsPdfService) {}

  ngOnInit() {
    console.log("init3");
  }

  printDemo() {
    this.jsPdfPrint.printListEmployeeToPayRoll();
  }

  demo() {
    // this.router.navigate(['nav/home'], { replaceUrl: true });
    //this.router.navigateByUrl('/home2');
    this.router.navigate(["nav/home/home2"], { replaceUrl: true });
  }
  select() {
    console.log("sd00");
  }
}
