import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-homepage2',
    templateUrl: './homepage2.component.html',
    styleUrls: ['./homepage2.component.scss'],
})
export class Homepage2Component implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {
        console.log('init2');
    }

    demo() {
        this.router.navigate(['nav/home/home3'], { replaceUrl: true });
        //this.router.navigateByUrl('home3');
        //this.router.navigate(['nav/home'],{ relativeTo: this.route });
    }
    select() {
        console.log('sd00');
    }
}
