import { Injectable } from '@angular/core';
import { StorageKey } from './storage.model';
import {
    IUserOnline,
    IProfileCompany,
} from '../../../Interfaces/interfacesPublics';

@Injectable({
    providedIn: 'root',
})
export class StorageService {
    private storage: Storage;

    constructor() {
        this.storage = localStorage;
        // console.log(this.storage.getItem(StorageKey.AUTH_TOKEN));

        // console.log(this.storage.getItem('demo'));
        // this.storage.clear();
    }

    //#region "AUTH"

    saveToken(token: string) {
        this.storage.setItem(StorageKey.AUTH_TOKEN, token);
    }
    getToken() {
        return this.storage.getItem(StorageKey.AUTH_TOKEN);
    }
    updateToken(token: string) {
        this.storage.removeItem(StorageKey.AUTH_TOKEN);
        this.storage.setItem(StorageKey.AUTH_TOKEN, token);
    }
    removeToken() {
        this.storage.removeItem(StorageKey.AUTH_TOKEN);
    }

    saveUserOnline(user: IUserOnline) {
        let value = JSON.stringify(user);
        this.storage.setItem(StorageKey.USER_ONLINE, value);
    }
    public getUserOnline(): IUserOnline {
        let value: string = this.storage.getItem(StorageKey.USER_ONLINE);
        if (value != '') {
        }
        let result: IUserOnline = JSON.parse(value);

        return result;
    }

    saveProfileCompany(company: IProfileCompany) {
        let value = JSON.stringify(company);
        this.storage.setItem(StorageKey.PROFILE_COMPANY, value);
    }
    getProfileCompany(): IProfileCompany {
        let value: string = this.storage.getItem(StorageKey.PROFILE_COMPANY);
        if (value != '') {
        }
        let result: IProfileCompany = JSON.parse(value);
        return result;
    }

    //#endregion

    // public save(key: StorageKey, value: any) {
    //     value = JSON.stringify(value);
    //     this.storage.setItem(key, value);
    // }

    public read(key: StorageKey): any {
        const value = this.storage.getItem(key);
        return JSON.parse(value);
    }

    // public remove(key: StorageKey) {
    //     return this.storage.removeItem(key);
    // }

    public clear() {
        this.storage.clear();
    }
}
