import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

import { NgxSpinnerService } from 'ngx-spinner';
@Injectable({
    providedIn: 'root',
})
export class LoadingService {
    constructor(private ngxShowLoader: NgxSpinnerService) {}
    public show() {
        this.ngxShowLoader.show();
    }

    public hide() {
        setTimeout(() => {
            this.ngxShowLoader.hide();
        }, 500);
    }
}
