import * as jsPDF from "jspdf";
import { StorageService } from "../storage/storage.service";

const storage = new StorageService();

class UtilsPrint {
  header() {}

  printDemo() {
    // Default export is a4 paper, portrait, using milimeters for units
    // var doc = new jsPDF();
    var doc = new jsPDF("p", "mm", "a4");

    //HEADER
    //doc = this.headTitleCompany(doc);
    doc.line(5, 30, 200, 30);

    //TITLE REPORT AND DATE
    doc = this.titleReport(doc);

    var blob = doc.output("blob");
    window.open(URL.createObjectURL(blob));
  }

  rectangles(doc: jsPDF, firtsLine: number) {
    doc.setLineWidth(0.7);
    //doc.line(30, 30, 560, 30);

    // doc.setDrawColor(200, 100, 0);
    //doc.setLineWidth(2);

    doc.setDrawColor(0);
    doc.setFillColor(255, 255, 255);
    doc.roundedRect(5, firtsLine, 200, 90, 3, 3, "FD");
    //doc.ellipse(50, 100, 90, 50);
    // doc.rect(10, 20, 150, 75);

    //doc.rect(5, 10, 196, 99); // empty red squar

    return doc;
  }

  textReceipt(doc: jsPDF, firtsLine: number) {
    doc.setFont("times");
    doc.setFontType("italic");
    doc.setFontSize(23);
    doc.setTextColor(0);
    doc.text(12, firtsLine, "Recibo No.  :");
    doc.setTextColor(98);
    doc.text(57, firtsLine, "50");
    doc.setTextColor(0);
    doc.text(119, firtsLine, "Bueno Por : ");
    doc.setTextColor(98);
    doc.text(162, firtsLine, "$90,000.00");

    firtsLine += 16;

    doc.setTextColor(0);
    doc.text(12, firtsLine, "Yo ");
    doc.setLineWidth(0.1);
    doc.line(28, firtsLine + 2, 150, firtsLine + 2);
    doc.setTextColor(98);
    doc.setFontSize(20);
    doc.text(28, firtsLine, "name");
    doc.setTextColor(0);
    doc.text(12, firtsLine + 10, "Recibi de ");
    doc.setLineWidth(0.1);
    doc.setTextColor(98);
    doc.text(45, firtsLine + 10, "Finca Negro Vargas ");
    doc.setLineWidth(0.1);
    doc.line(44, firtsLine + 12, 145, firtsLine + 12);
    doc.setTextColor(0);
    doc.text(150, firtsLine + 12, "La cantidad de  ");
    doc.setTextColor(98);
    doc.setFontSize(17);
    doc.text(12, firtsLine + 20, "cinco mil quinientos ochenta pesos 00/00");
    doc.setLineWidth(0.1);
    doc.line(12, firtsLine + 22, 140, firtsLine + 22);
    doc.setFontSize(20);
    doc.setTextColor(0);
    doc.text(145, firtsLine + 22, "Por conceptor de   ");
    doc.setFontSize(17);
    doc.setTextColor(98);
    doc.text(12, firtsLine + 29, "Regalia Pascual  ");
    doc.setLineWidth(0.1);
    doc.setLineWidth(0.1);
    doc.line(12, firtsLine + 31, 140, firtsLine + 31);

    doc.setFontSize(20);
    doc.setTextColor(0);
    doc.text(12, firtsLine + 39, "En Fecha :");

    doc.setFontSize(16);
    doc.setTextColor(98);
    doc.text(46, firtsLine + 39, "Martes 25 ,de Febrero 2019");

    doc.setLineWidth(0.1);
    doc.line(45, firtsLine + 41, 116, firtsLine + 41);

    doc.setLineWidth(0.1);
    doc.line(200, firtsLine + 53, 108, firtsLine + 53);
    doc.setFontSize(15);
    doc.setTextColor(98);
    doc.text(150, firtsLine + 59, "Firma");

    // JsBarcode("#itf", "85454584559", { format: "CODE128" });
    // const img: any = document.querySelector("img#itf");
    // doc.addImage(img.src, "JPEG", 10, firtsLine + 47, 37, 15);

    return doc;
  }

  // textToBase64Barcode(text) {
  //   var canvas = document.createElement("canvas");
  //   JsBarcode(canvas, text, { format: "CODE39" });
  //   return canvas.toDataURL("image/png");
  // }

  //#region UTils Reports

  headerTitleCompanyCenter(doc: jsPDF): string {
    let sd: string = storage.getToken();

    doc.setFont("courier");
    doc.setFontType("bold");
    doc.setFontSize(19);

    //var to insert
    var textPrint = "";
    let multNextLIne: number = 0;
    let nextBr = 0;
    let separatorLine: number = 0;
    let maxLenth = 0;
    let numberSecLine = 0;
    let primaryLine = 20;

    let lineSalt = 0;

    //name company
    textPrint = "Finca Negro Vargas";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, primaryLine, 200);
    multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    lineSalt = multNextLIne;
    nextBr = 4 * lineSalt;
    separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    //Slogan Company
    doc.setFontSize(10);
    textPrint = "Bananos Organicos ";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);
    multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);

    lineSalt += multNextLIne;
    nextBr = 4 * lineSalt;
    separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    doc.setFontType("normal");
    textPrint = "Calle de Ejmplo Nro x ";
    maxLenth = 40;
    doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);

    //multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    // nextBr = nextBr++ * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    // //Adrress Company
    // doc.setFontSize(9);
    // doc.setFontType("normal");
    // textPrint = "Direccion de Empresa x sa sadsadsadsad asdsadsad   ";
    // maxLenth = 40;
    // numberSecLine++;
    // doc = this.centerText(
    //   doc,
    //   textPrint,
    //   maxLenth,
    //   primaryLine + separatorLine,
    //   200
    // );

    // multNextLIne = this.calculateLine(textPrint, maxLenth);
    // nextBr = 5 * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    return doc;
  }
  headerTitleCompanyLeft(doc: jsPDF): string {
    let sd: string = storage.getToken();

    doc.setFont("Arial");
    //doc.setFontType("bold");
    doc.setFontSize(18);

    //var to insert
    var textPrint = "";
    let multNextLIne: number = 0;
    let nextBr = 0;
    let separatorLine: number = 0;
    let maxLenth = 0;
    let numberSecLine = 0;
    let primaryLine = 20;

    let lineSalt = 0;

    //name company
    textPrint = "Finca Negro Vargas";
    doc.text(7, 20, textPrint);

    // maxLenth = 40;
    // doc = this.centerText(doc, textPrint, maxLenth, primaryLine, 200);
    // multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    // lineSalt = multNextLIne;
    // nextBr = 4 * lineSalt;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    //Slogan Company
    doc.setFontSize(10);
    textPrint = "Bananos Organicos ";
    doc.text(7, 25, textPrint);
    // maxLenth = 40;
    // doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);
    // multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);

    // lineSalt += multNextLIne;
    // nextBr = 4 * lineSalt;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr + primaryLine;

    // doc.setFontType("normal");
    // textPrint = "Calle de Ejmplo Nro x ";
    // maxLenth = 40;
    // doc = this.centerText(doc, textPrint, maxLenth, separatorLine, 200);

    //multNextLIne = this.calculateLine(textPrint, maxLenth, nextBr);
    // nextBr = nextBr++ * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    // //Adrress Company
    // doc.setFontSize(9);
    // doc.setFontType("normal");
    // textPrint = "Direccion de Empresa x sa sadsadsadsad asdsadsad   ";
    // maxLenth = 40;
    // numberSecLine++;
    // doc = this.centerText(
    //   doc,
    //   textPrint,
    //   maxLenth,
    //   primaryLine + separatorLine,
    //   200
    // );

    // multNextLIne = this.calculateLine(textPrint, maxLenth);
    // nextBr = 5 * numberSecLine;
    // separatorLine = multNextLIne == 0 ? nextBr : nextBr * multNextLIne;

    return doc;
  }

  lineHorizontal(doc: jsPDF, numberLines: number) {
    doc = doc.line(5, numberLines, 200, numberLines);

    return doc;
  }

  dateRight(doc: jsPDF) {
    doc.setFont("Consolas");
    //doc.setFontType("bold");
    doc.setFontSize(10);

    //var to insert
    var textPrint = "";

    //name company
    textPrint = "21-Sept-2020 ";
    doc.text(155, 28, textPrint);

    return doc;
  }

  titleRptLeft(doc: jsPDF) {
    //doc.addFont("ComicSansMS", "Comic Sans", "normal");
    doc.setFont("helvetica");
    doc.setFontType("bold");
    doc.setFontSize(10);

    //var to insert
    var textPrint = "";

    //name company
    textPrint = "Reporte de X cosa Largo  ";
    doc.text(15, 35, textPrint);

    return doc;
  }

  //#endregion

  titleReport(doc: jsPDF): string {
    doc.setFont("times");
    doc.setFontType("italic");
    doc.setFontSize(11);

    doc = this.centerText(doc, "Reporte x Cosa Largo ", 40, 35, 200);
    //doc = this.centerText(doc, "", 40, 35, 300);

    return doc;
  }

  centerText(
    doc: jsPDF,
    text: string,
    maxLength: number,
    topPosition: number,
    pdfWidth: number
  ) {
    let lineTop: number = 500;
    // var lMargin = 15; //left margin in mm
    // var rMargin = 15; //right margin in mm
    //pdfInMM = 210; // width of A4 in mm
    var pageCenter = pdfWidth / 2;

    //var doc = new jsPDF("p", "mm", "a4");
    // var paragraph = "";

    let countLength = 0;
    let newText: string = "";
    for (let i = 0; i < text.length; i++) {
      //console.log(text.charAt(i));
      newText += text.charAt(i);
      if (countLength == maxLength) {
        newText += "\n\n";
        countLength = 0;
      }
      countLength++;
    }
    //console.log(newText);

    // var lines = doc.splitTextToSize(newText, pdfInMM - lMargin - rMargin);

    var lines = newText.split("\n\n");
    var dim = doc.getTextDimensions("Text");
    var lineHeight = dim.h;

    for (var i = 0; i < lines.length; i++) {
      lineTop = (lineHeight / 1.5) * i;
      doc.text(lines[i], pageCenter, topPosition + lineTop, "center"); //see this line
    }
    return doc;
  }

  calculateLine(text: string, maxWidth: number, nexBr: number) {
    // let lines= text.split()
    let countLength = 0;

    let newText: string = "";
    for (let i = 0; i < text.length; i++) {
      //console.log(text.charAt(i));
      newText += text.charAt(i);
      if (countLength > maxWidth) {
        newText += "\n\n";
        countLength = 0;
      }
      countLength++;
    }

    let longitud: any[];
    longitud = newText.split("\n\n");
    if (longitud.length == 0) {
      return 1;
    } else {
      return longitud.length;
    }
  }

  createReceipt() {
    // var doc = new jsPDF();
    var doc = new jsPDF("p", "mm", "a4");
    doc = this.rectangles(doc, 20);

    doc = this.textReceipt(doc, 30);

    doc = this.rectangles(doc, 140);

    doc = this.textReceipt(doc, 150);

    //doc = this.textReceipt(doc);

    //doc.text(20, 10, "value");
    //doc.addPage();

    // let array: Array<number> = [1, 2];
    // for (let i of array) {
    //   doc.text(20, 10, "value");

    //   if (i != array.length) {
    //     doc.addPage();
    //   }
    // }

    var blob = doc.output("blob");
    window.open(URL.createObjectURL(blob));
  }
  //#region "AUTH"

  //#endregion
}

export default UtilsPrint;
