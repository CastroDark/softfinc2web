import UtilsPrint from "../utilsPrint";
const functUtils = new UtilsPrint();
import * as jsPDF from "jspdf";

class ListEmpPayRoll {
  print(doc: jsPDF) {
    //doc = jsService.titleReport(doc);
    //HEADER
    doc = functUtils.headerTitleCompanyLeft(doc);

    doc = functUtils.dateRight(doc);

    doc = functUtils.titleRptLeft(doc);

    doc = functUtils.lineHorizontal(doc, 38);

    return doc;
  }
}

export default ListEmpPayRoll;
