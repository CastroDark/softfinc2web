import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavModule } from "./core/components/nav/nav.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { HttpConfigInterceptor } from "./core/services/http/interceptor.service";

//Services Import providers
import { HttpApiService } from "./core/services/http/httpApi.service";
import { NgxSpinnerModule } from "ngx-spinner";
import { LoadingService } from "./core/services/loadingSpinner/loading.service";
import { JsPdfService } from "./core/services/print/jspdf.service";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NavModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },
    HttpApiService,
    LoadingService,
    JsPdfService
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
